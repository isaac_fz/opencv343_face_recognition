Opencv-3.4.3版本
 一、主要实现功能
 1、实现人脸识别
 2、实现人脸关键点标识
 3、实现人脸对齐
 4、实现人脸训练变量检测
二、项目结构
	business -- 业务目录
	entity	 -- 实体类
	frame    -- 摄像头显示窗口
	service	 -- 业务服务接口
	util     -- 人脸识别工具类(类中有相关方法的说明)
	StartDemo主程序运行类	
三、后续业务结合
1.会员图片的添加要单独去添加。
	1.1在添加的时候进行人脸识别，同时截取头像。（检测是否满足要求,不满足则不允许进行加入）。
	1.2判断新加入的人员头像是否存在，存在则提示。
	1.2加入后进行训练。
2.添加人脸识别和截取功能。
3.图片上传，可以本地，远程加载训练。 

eclipse运行
	1、先配置 conf/config.conf文件中 saveFacePath、saveOriFacePath、trainModelPath 三个路径的位置。
	2、将faceimg照片放入到saveFacePath路径下
	3、运行StartDemo主程序运行类	

项目部署运行
	1、先配置 conf/config.conf文件中 saveFacePath、saveOriFacePath、trainModelPath 三个路径的位置。
	2、将faceimg照片放入到saveFacePath路径下
	3、引入的springboot的相关包，是为了打jar包。
	4、运行前修改 util/PathUtil下的getFilePath(String path)将下面的代码注释掉
	 	path = PathUtil.getClassResources().concat(path);
	5、将resources下的face目录和文件放到和jar同级别目录下即可。
	6、确保电脑上有摄像头。
	 
