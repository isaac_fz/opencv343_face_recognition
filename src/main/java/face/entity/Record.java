package face.entity;

public class Record{
	
	private String id;
	private String buyGoodsTime;
	private String goodsName;
	private String goodsPrice;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBuyGoodsTime() {
		return buyGoodsTime;
	}
	public void setBuyGoodsTime(String buyGoodsTime) {
		this.buyGoodsTime = buyGoodsTime;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsPrice() {
		return goodsPrice;
	}
	public void setGoodsPrice(String goodsPrice) {
		this.goodsPrice = goodsPrice;
	} 
}
