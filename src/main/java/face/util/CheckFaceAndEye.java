package face.util;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;

/**
 * 图片面部检测 - 查看摄像头中是否有人脸
 * @author ShiQiang
 *
 */
public class CheckFaceAndEye { 
	//正面人脸
	static CascadeClassifier faceDetector = new CascadeClassifier(PathUtil.getFilePath(Common.faceDetectPath));
	//眼睛
	static CascadeClassifier eyesDetector = new CascadeClassifier(PathUtil.getFilePath(Common.eyesCascadePath));
	
	/**
	 * 检测是否有正面人脸
	 * @param image
	 * @return boolean
	 */
	public static boolean checkHasFace(Mat image) {
		RectVector faces = findFaces(image);
		boolean flag =  faces != null;
		faces.clear();
		return flag;
	}
	
	/**
	 * 检测是否有人的眼睛
	 * @param image
	 * @return boolean
	 */
	public static boolean checkHasEye(Mat image) {
		RectVector eyes = findEyes(image);
		boolean flag =  eyes != null;
		eyes.clear();
		return flag;
	}
	
	/**
	 * 获取人脸数据
	 * @param image
	 * @return RectVector
	 */
	public static RectVector findFaces(Mat image) {  
		Mat imageGray = FaceAndEyeToos.doColorHist(image); 
		//进行人脸识别
		RectVector faceDetections = new RectVector(); 
		
		faceDetector.detectMultiScale(imageGray, faceDetections);
		if(faceDetections.empty()){
			return null;
		}   
		
		return faceDetections;
	}
	
	/**
	 * 获取人眼数据
	 * @param image
	 * @return RectVector
	 */
	public static RectVector findEyes(Mat image) { 
		Mat imageGray = FaceAndEyeToos.doColorHist(image); 
		// 存储找到的眼睛矩形。
		RectVector eyes = new RectVector();
		
		eyesDetector.detectMultiScale(imageGray, eyes); 
		if(eyes.empty()){
			return null;
		} 
		
		return eyes;
	}
}
