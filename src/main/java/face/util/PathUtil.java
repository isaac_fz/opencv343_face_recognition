package face.util;

import java.io.File;

/** 
 * 说明：路径工具类
 * 
 * 修改时间：2014年9月20日
 * @version
 */
public class PathUtil { 
	 
	/**获取classpath1
	 * @return
	 */
	public static String getClasspath(){
		String path = (String.valueOf(Thread.currentThread().getContextClassLoader().getResource(""))+"../../").replaceAll("file:/", "").replaceAll("%20", " ").trim();	
		if(path.indexOf(":") != 1){
			path = File.separator + path;
		}
		return path;
	}
	
	/**获取classpath2
	 * @return
	 */
	public static String getClassResources(){
		String path =  (String.valueOf(Thread.currentThread().getContextClassLoader().getResource(""))).replaceAll("file:/", "").replaceAll("%20", " ").trim();	
		if(path.indexOf(":") != 1){
			path = File.separator + path;
		}
		return path;
	} 
	
	 
	public static String getFilePath(String path){
		path = PathUtil.getClassResources().concat(path);
		System.out.println("加载文件路径:"+path);
		return path;
	} 
	public static void main(String[] args) {
		System.out.println("是否存在文件"+new File("E:/code/workspace/face/target/face-0.0.1-SNAPSHOT.jar/BOOT-INF/classes/face/invalid.jpg").isFile());
	}
}
